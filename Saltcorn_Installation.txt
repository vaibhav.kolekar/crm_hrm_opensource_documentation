Quick automated install
This has been tested on Debian 9, 10 and 11 and Ubuntu 18.04, 20.04 and 21.04. All you need is to run these three lines on the command line shell, as root or as a user with sudo access:

wget -qO - https://deb.nodesource.com/setup_14.x | sudo bash -
sudo apt-get install -qqy nodejs
npx saltcorn-install -y
The first two lines will install Node.js 14. The last line will call the Saltcorn install script accepting all the defaults, Which installs PostgreSQL and sets up Saltcorn as a service listening on port 80.

If you want a different port or a different database backend, or not install as a service, You can omit the final -y to get an interactive installation:

wget -qO - https://deb.nodesource.com/setup_14.x | sudo bash -
sudo apt-get install -qqy nodejs
npx saltcorn-install